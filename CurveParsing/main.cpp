#include "CurveParsing.h"
#include <pcl/visualization/pcl_visualizer.h>

void getFrameTime(const char* src, vector<float>& vOut)
{
	fstream fp;
	fp.open( src );
	string line;
	int i = 0;
	while(getline(fp, line))
	{
		int iFindBegin ;
		int iFindEnd = line.find("\t");
		int iIndex = stoi( line.substr( 0, iFindEnd ) );
		iFindBegin = iFindEnd;
		iFindEnd = line.find("\n");
		float fTime = stof( line.substr( iFindBegin+1, iFindEnd ) );
		vOut.push_back(fTime);
		//cout << "* iIndex : " << iIndex << " fTime : " << vOut.back() << endl;
		i++;
	}
	fp.close();
	;
}

void readAccelerometer(const char* src, Eigen::Vector3f& v)
{
	fstream fp;
	fp.open( src );
	string line;
	int i = 0;
	while(getline(fp, line))
	{
		v(i) = stof(line);
		//cout << v(i) << endl;
		i++;
	}
	fp.close();
}

void gravityCalibrated(Eigen::Vector3f& vIn, Eigen::Matrix3f& mOut)
{
	Eigen::Vector3f b(0, -1, 0);
	if( abs(vIn(1)) > 1)
	{
		Eigen::Matrix3f R;
		R << 1, 0, 0, 0, 1, 0, 0, 0, 1;
		mOut = R;
	}
	else
	{
		Eigen::Vector3f v = vIn.normalized().cross(b);
		//Eigen::Vector3f v = vIn.cross(b);
		float s = v.norm();
		//float c = vIn.dot(b);
		float c = vIn.normalized().dot(b);
		Eigen::Matrix3f R;
		Eigen::Matrix3f I;
		Eigen::Matrix3f vx;
		vx << 0, -v(2), v(1), v(2), 0, -v(0), -v(1), v(0), 0;
		//std::cout << "* vx : " << std::endl;
		//std::cout << vx << std::endl;
		I << 1, 0, 0, 0, 1, 0, 0, 0, 1;
		R << 0, 0, 0, 0, 0, 0, 0, 0, 0;
		R = I + vx + vx*vx*((1-c)/(s*s));
		std::cout << "* R*vIn : " << std::endl;
		std::cout << R*vIn << std::endl;
		mOut = R;
	}
}

int main(int argc, char** argv)
{
	stringstream ss;
	std::pair<int, int> seq;
	string sFileDir;
	std::pair<string, string> sFileName;

	//	test cmd
	//	"D://2015-02-02-08-21-03/" "6&278E9DD&0&4_CandidatePoints.txt" "6&D2B1927&0&2_CandidatePoints.txt"
	
	if (argc > 1)
	{
		sFileDir			= argv[1];
		sFileName.first		= argv[2];
		sFileName.second	= argv[3];
		//seq.first  = atoi(argv[2]);
		//seq.second = atoi(argv[3]);
	}
	else
	{
		cout << "Input format : " << endl;
		cout << "CurveParsing.exe <DIR> <File.txt> <File.txt>" << endl << endl;
		cout << "Note : Need to use <\"your dir\">" << endl ;
		return -1;
	}
	cout << "! " << endl;
	
	vector<int> vIndex1;
	vector<int> vIndex2;
	vector<float> vTime1;
	vector<float> vTime2;
	vector< std::pair<int,int> > vResult1;
	vector< std::pair<int,int> > vResult2;
	vector< std::pair<int,int> > vPairs;
	vector< std::pair<int,int> > vPairs2;
	pcl::PointCloud<pcl::PointXYZ>::Ptr pcCurve1 (new pcl::PointCloud<pcl::PointXYZ>());
	pcl::PointCloud<pcl::PointXYZ>::Ptr pcCurve2 (new pcl::PointCloud<pcl::PointXYZ>());
	pcl::PointCloud<pcl::PointXYZ>::Ptr pcResult1 ( new pcl::PointCloud<pcl::PointXYZ>());
	pcl::PointCloud<pcl::PointXYZ>::Ptr pcResult2 ( new pcl::PointCloud<pcl::PointXYZ>());

	
	


	Eigen::Vector3f acc1;
	Eigen::Vector3f acc2;
	ss.str("");
	ss << sFileDir << sFileName.first << ".txt";
	readAccelerometer(ss.str().c_str(), acc1);
	ss.str("");
	ss << sFileDir << sFileName.second << ".txt";
	readAccelerometer(ss.str().c_str(), acc2);
	cout << "*  acc1 : " << acc1 << endl;
	cout << "*  acc2 : " << acc2 << endl;
	Eigen::Matrix3f g1;
	Eigen::Matrix3f g2;
	g1.Zero();
	g2.Zero();
	gravityCalibrated(acc1, g1);
	gravityCalibrated(acc2, g2);

	ss.str("");
	ss << sFileDir << sFileName.first << "_frameTime.txt" ;
	getFrameTime(ss.str().c_str(), vTime1);
	ss.str("");
	ss << sFileDir << sFileName.second << "_frameTime.txt" ;
	getFrameTime(ss.str().c_str(), vTime2);


	ss.str("");
	ss << sFileDir << sFileName.first << "_CandidatePoints.txt" ;
	CurveParsing* cp = new CurveParsing();
	cp->read(ss.str().c_str(), vIndex1, pcCurve1);
	cp->removeRedundant(vIndex1, pcCurve1);
	//cp->getSeq(1,vResult1, pcResult1);
	cp->getPair(vPairs);

	ss.str("");
	ss << sFileDir << sFileName.second << "_CandidatePoints.txt" ;
	CurveParsing* cp2 = new CurveParsing();
	cp2->read(ss.str().c_str(), vIndex2, pcCurve2);
	cp2->removeRedundant(vIndex2, pcCurve2);
	//cp2->getSeq(1,vResult2, pcResult2);
	cp2->getPair(vPairs2);


	vector<pair<int, int>> matched_tmp;
	// comapre pairs <aBegin, aEnd > <bBegin, bEnd>
	// compare the nearest seq distance

	if ( vPairs.size() ==0 || vPairs2.size() == 0)
	{
		cout << "error match : no sequence in it." << endl;
		cout << "vPairs.size() : " << vPairs.size() << endl;
		cout << "vPairs2.size() : " << vPairs2.size() << endl;
		system("pause");
		return 0;
	}
	for(int i = 0; i < vPairs.size(); i++)
	{
		pair<int, int> a = vPairs.at(i);
		double distance = 9999;
		int match_index;
		for(int j = 0; j < vPairs2.size(); j++)
		{
			pair<int, int> b = vPairs2.at(j);
			if (abs(a.first - b.first) < distance)
			{
				distance	= abs(a.first - b.first);
				match_index = j;
				//cout << " match : " << "(" << i << ", " << match_index << ")" << endl;
			}
		}
		matched_tmp.push_back(pair<int,int>(i,match_index));
	}
	
	// remove duplicate seq pairs
	vector<pair<int, int>>::iterator it_min ;
	vector<pair<int, int>> matched;
	for(vector<pair<int, int>>::iterator it = matched_tmp.begin(); it != matched_tmp.end(); ++it)
	{
		if (it == matched_tmp.begin())
		{
			it_min = it;
		}
		if( it+1 != matched_tmp.end() && it->second == (it+1)->second)
		{
			int distance1 = abs(vPairs.at(it->first).first - vPairs2.at(it->second).first );
			int distance2 = abs(vPairs.at((it+1)->first).first - vPairs2.at((it+1)->second).first) ;
			if( distance1 < distance2)
			{it_min = it;}
			else
			{it_min = (it+1);}
		}
		else if(it+1 == matched_tmp.end())
		{
			//cout << "* it_min : " << "(" << it_min->first << ", " << it_min->second << ")" << endl;
			matched.push_back(*it_min);
			break;
		}
		else
		{
			//cout << "case4" << endl;
			//cout << "* it_min : " << "(" << it_min->first << ", " << it_min->second << ")" << endl;
			matched.push_back(*it_min);
			it_min = it;
		}
	}

	fstream fp;
	string sName = "curve_pairs.txt";
	cout << "* create file : " << sFileDir + sName << endl;
	fp.open(sFileDir + sName, ios::out);
	for( vector<pair<int, int>>::iterator it = matched.begin(); it != matched.end(); ++it )
	{
		cout << "* Matched : " << "(" << it->first << ", " << it->second << ")" << endl;
		fp << it->first   << "\t" << it->second << endl;
		//cout << vPairs.at(it->first).first   << "\t" << vPairs.at(it->first).second   << "\t" 
		//   << vPairs2.at(it->second).first << "\t" << vPairs2.at(it->second).second << endl;
		//cout << vPairs.at(0).first   << "\t" << vPairs.at(0).second   << endl;
		//cout << vPairs.at(1).first   << "\t" << vPairs.at(1).second  <<endl;
	}
	fp.close();



	//pcl::visualization::PCLVisualizer::Ptr viewer(new pcl::visualization::PCLVisualizer);
	//pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> color_r(pcResult1, 255, 0, 0);
	//pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> color_g(pcResult2, 0, 255, 0);
	//viewer->addCoordinateSystem(1000.0);
	//viewer->addPointCloud(pcResult1, color_r, "pcResult1");
	//viewer->addPointCloud(pcResult2, color_g, "pcResult2");

	//
	//while(!viewer->wasStopped())
	//{
	//	viewer->spinOnce (100);
	//	boost::this_thread::sleep (boost::posix_time::microseconds (100000));
	//}


	//cp->getPair(vPairs);
	//cp->printResult();
	

	//vector<int> vIndex2;
	//vector<float> vTime2;
	//vector< std::pair<int,int> > vResult2;
	//vector< std::pair<int,int> > vPairs2;
	//pcl::PointCloud<pcl::PointXYZ>::Ptr pcCurve2 (new pcl::PointCloud<pcl::PointXYZ>());
	//pcl::PointCloud<pcl::PointXYZ>::Ptr pcResult2 ( new pcl::PointCloud<pcl::PointXYZ>());

	//ss.str("");
	//ss << sFileDir << sFileName.second << "_CandidatePoints.txt" ;
	//CurveParsing* cp2 = new CurveParsing();
	//cp2->read(ss.str().c_str(), vIndex2, pcCurve2);
	//cp2->removeRedundant(vIndex2, pcCurve2);
	//cp2->getPair(vPairs2);
	//cp2->printResult();
	//ss.str("");
	//ss << sFileDir << sFileName.second << "_frameTime.txt" ;
	//getFrameTime(ss.str().c_str(), vTime2);


	//
	//system("pause");
	return 0;
}