#include "CurveParsing.h"

void CurveParsing::read(const char* _fileName, vector<int>& _vIndice, pcl::PointCloud<pcl::PointXYZ>::Ptr _pPointCloud )
{
	
	fstream fp;
	fp.open( _fileName );
	string line;
	
	if(!fp.is_open())
		cout << "* OOPS! : "  << "Cant Open File. " << endl;
	else 
	{
		cout << "*  CurveParsing::read()..." <<endl;
		cout << _fileName <<endl;
	}
	while( getline(fp, line) )
	{
		int iFindBegin;
		int iFindEnd;
		int iIndex;
		pcl::PointXYZ pPoint;

		iFindEnd = line.find("\t");
		iIndex = stoi( line.substr( 0, iFindEnd ) );
		iFindBegin = iFindEnd ;
		//cout << "Read iIndex: " << iIndex << endl; 
		iFindEnd = line.find("\t", iFindBegin+1);
		pPoint.x  = stof( line.substr( iFindBegin+1, iFindEnd ) );
		iFindBegin = iFindEnd ;
		iFindEnd = line.find("\t", iFindBegin+1);
		pPoint.y  = stof( line.substr( iFindBegin+1, iFindEnd));
		iFindBegin = iFindEnd ;
		iFindEnd = line.find("\t", iFindBegin+1);
		pPoint.z  = stof( line.substr( iFindBegin+1, iFindEnd));
		//cout << "Read pPoint: " << pPoint.x << ", " << pPoint.y << ", " << pPoint.z << endl; 
		if ( pPoint.z != 0)
		{
			_vIndice.push_back(iIndex);
			_pPointCloud->push_back(pPoint);
		}
		
		
	}
	fp.close();
}



void CurveParsing::run( vector<int>& _vIndice, pcl::PointCloud<pcl::PointXYZ>::Ptr _pPointCloud )
{
	int _IndexStart;
	int _IndexEnd;
	int _IndexPre;
	int _WindowSize = 8;
	int count = 0;
	for( int i = 0; i <  _pPointCloud->size(); i++)
	{
		//cout << " index : " << _vIndice.at(i) << " x : " << _pPointCloud->at(i).x << " y : " << _pPointCloud->at(i).y << " z: " << _pPointCloud->at(i).z << endl;
		//cout << " _IndexStart : " << _IndexStart << endl;
		//cout << " _IndexEnd : " << _IndexPre << endl;
		if (i != 0)
		{
			if( _vIndice.at(i) - _IndexPre < _WindowSize)
			{
				if( i == _pPointCloud->size()-1)
				{
					_IndexPre = _vIndice.at(i);
					std::pair<int, int> _piar( _IndexStart, _IndexPre);
					mvIndexPairs.push_back( _piar);
				}
				//cout << "* case 1" <<endl;
				//cout << _vIndice.at(i) <<endl;
				//cout << " _vIndice.at(i) - _IndexPre : " << _vIndice.at(i) - _IndexPre << endl;
				_IndexPre = _vIndice.at(i);
				count++;
			}
			else 
			{
				//cout << "* case 2" <<endl;
				//cout << _vIndice.at(i) <<endl;
				//cout << " _vIndice.at(i) - _IndexPre : " << _vIndice.at(i) - _IndexPre << endl;
				if( count >= 5 )
				{
					std::pair<int, int> _piar( _IndexStart, _IndexPre);
					//cout << "**" << _piar.first << ", " << _piar.second<< endl;
					mvIndexPairs.push_back( _piar);
				}
				_IndexStart = _vIndice.at(i);
				_IndexPre	= _vIndice.at(i);
				count = 0;
			}
		}
		else
		{
			//cout << "* case 3" <<endl;
			_IndexStart = _vIndice.at(i);
			_IndexPre = _IndexStart;
			//_IndexEnd	= _vIndice.at(i);
		}
	}


	cout << "* processing ..."<<endl;
	float direction_current;
	float direction_previous;
	for(int i = 0; i < mvIndexPairs.size(); i++)
	{
		int iBegin = mvIndexPairs.at(i).first;
		int iEnd = mvIndexPairs.at(i).second;
		pcl::PointXYZ p1;
		pcl::PointXYZ p2;
		pcl::PointXYZ p3;
		cout << "* iBegin : " << iBegin << " iEnd : " << iEnd << endl;
		for ( int j = 0; j < mpcCurve->size(); j++)
		{
			cout << "* mvIndex : " << mvIndex.size() <<endl;
			if (mvIndex.at(j) >= iBegin && mvIndex.at(j) < iEnd)
			{
					
				p1.getVector3fMap() = mpcCurve->at(j).getVector3fMap();
				p2.getVector3fMap() = mpcCurve->at(j+1).getVector3fMap();
				p3.getVector3fMap() = p2.getVector3fMap() - p1.getVector3fMap();
				if( p3.y <= 0 )
					direction_current = 1;
				else 
					direction_current = -1;
				//cout << "* mvIndex.at(j) : " << mvIndex.at(j) << " mvIndex.at(j+1) : " << mvIndex.at(j+1) << endl;
				//cout << "* direction_previous : " <<  direction_previous << endl;
				//cout << "* direction_current  : " <<  direction_current << endl;
				if ( direction_previous == -1 && direction_current == 1 ) //反彈
				{
					cout << "* 反彈 case : " << endl;
					iEnd = mvIndex.at(j);
					//cout << "- iBegin : " << iBegin << " iEnd : " << iEnd << endl;
					mvIndexPairs2.push_back(std::pair<int,int>(iBegin, iEnd));
					for( int k = 0; k < mpcCurve->size(); k++)
					{
						if (mvIndex.at(k) >= iBegin && mvIndex.at(k) <= iEnd)
						{
							//cout << mvIndex.at(k) << endl;
							mvIndex2.push_back(mvIndex.at(k));
							mpcCurve2->push_back(mpcCurve->at(k));
						}
					}
					break;
				}
				else if (mvIndex.at(j+1) == iEnd)
				{
					cout << "* NOT 反彈 case : " << endl;
					mvIndexPairs2.push_back(std::pair<int,int>(iBegin, iEnd));
					for( int k = 0; k < mpcCurve->size(); k++)
					{
						if (mvIndex.at(k) >= iBegin && mvIndex.at(k) <= iEnd){
							//cout << "* mvIndex.at(k) : " << mvIndex.at(k) << endl;
							int iTMP = mvIndex.at(k);
							mvIndex2.push_back(iTMP);
							pcl::PointXYZ p ;
							p.getVector3fMap()= mpcCurve->at(k).getVector3fMap();
							mpcCurve2->push_back(p);
						}
					}
				}
			}
			direction_previous = direction_current;
		}

		//direction_previous = direction_current;
	}
	
}

void CurveParsing::printResult()
{
	cout << "mvIndexPairs.size : " <<  mvIndexPairs2.size() << endl;
	if (mvIndexPairs2.size() != 0)
	{
		for ( int i = 0; i < mvIndexPairs2.size(); i++)
		{
			std::cout << mvIndexPairs2.at(i).first << ", " << mvIndexPairs2.at(i).second << endl; 
		}
	}
}

void CurveParsing::removeRedundant( vector<int>& _vIndice, pcl::PointCloud<pcl::PointXYZ>::Ptr _pPointCloud )
{
	std::cout << "* removeRedundant..." << endl; 
	std::cout << "* points before : " << _vIndice.size() << endl; 
	// 1. 目的 : 把球體影像碎片重心合併
	int i = 0;
	int j = 0;
	int _index	  = _vIndice.at(j);
	int _indexPre = _vIndice.at(i);
	//std::cout << "_vIndice.size() : " << _vIndice.size() << endl;
	while( j < _vIndice.size())
	{

		//////////////////////////////////////////
		//			1.合併&移除多餘的點			//
		//////////////////////////////////////////

		// 找坐落在同frame上的點的index(i, j)
		
		//std::cout << "*j值 : " << j << " , _vIndice.at(j): " << _vIndice.at(j) << endl;

		// 1a. 找相同的index頭(i)尾(j)
		while ( true )
		{
			if ( _indexPre - _index == 0 )
			{
				//std::cout << "*a i : " << i << " ,j : " << j << endl;
				//std::cout << "*a _indexPre : " << _indexPre << " ,_index : " << _index << endl;
				j++;
				if (j == _vIndice.size() -1)
					break;
				_index = _vIndice.at(j);
			}
			else
			{
				//std::cout << "*b i : " << i << " ,j : " << j << endl;
				j--;
				_index = _vIndice.at(j);
				break;
			}
			
		}	

		//std::cout << "*c i : " << i << " ,j : " << j << endl;
		// 1.b 若為同區塊則加總平均，不同則不理會
		if ( j - i >= 1) // 若兩個點以上
		{
			//std::cout << "* 有重複點兩個點以上 " << _vIndice.at(j) << endl;
			pcl::PointXYZ p;
			int count = 1;
			for (int k = i; k <= j; k++)
			{
				// 相同區塊的第一個區塊
				if ( k == i)
				{
					//std::cout << "*  k == i " << endl;
					//std::cout << "*mvIndex.size()-1 : " << mvIndex.size()-1 << " , _vIndice.at(i): " <<  _vIndice.at(i) << endl;
					//std::cout << "*mpcCurve->at(mpcCurve->size()-1).z : " << mpcCurve->at(mpcCurve->size()-1).z << " ,_pPointCloud->at(i).z: " <<  _pPointCloud->at(i).z << endl;
					//std::cout << "*abs : " << abs(mpcCurve->at(mpcCurve->size()-1).z - _pPointCloud->at(i).z) << " ,_pPointCloud->at(i).z* 0.03: " <<  _pPointCloud->at(i).z* 0.03 << endl;

					float threshold;
					if (_pPointCloud->at(i).z < 1000)
						threshold = 0.25;
					else if (_pPointCloud->at(i).z < 2000)
						threshold = 0.15;
					else 
						threshold = 0.1;
					// 若與前一個frame的球位置接近則判定為合理的區塊
					// 前一的位置 : mpcCurve->at(mpcCurve->size()-1)
					if ( mvIndex.size() > 0 &&  
						 abs(mvIndex.at(mvIndex.size()-1) - _vIndice.at(i)) <= 3 &&
						 abs(mpcCurve->at(mpcCurve->size()-1).z - _pPointCloud->at(i).z) < _pPointCloud->at(i).z* threshold)
					{
						//cout << "create points... " << k << " index " << _vIndice.at(k) << endl;
						p.x = _pPointCloud->at(i).x;
						p.y = _pPointCloud->at(i).y;
						p.z = _pPointCloud->at(i).z;
					}
					// 否則找下一個為起始的區塊
					else
					{
						if(mvIndex.size() > 0)
						{
							//cout << "mvIndex... " << mvIndex.size() << " abs " << abs(mvIndex.at(mvIndex.size()-1) - _vIndice.at(i)) << endl;
						}
						p.x = 0;
						p.y = 0;
						p.z = 0;
						i++;
						//cout << "move points... " << k << " to " << i << endl;
					}
				}
				// 若要合併的區塊深度值合理
				else if ( k != i && abs((_pPointCloud->at(k).z - _pPointCloud->at(i).z)) < _pPointCloud->at(i).z * 0.03 )
				{
					//cout << "merging points... " << _vIndice.at(k) << endl;
					//cout << "i " << i << " j " << j << " k " << k << endl;
					p.x += _pPointCloud->at(k).x;
					p.y += _pPointCloud->at(k).y;
					p.z += _pPointCloud->at(k).z;
					count++;
				}
			}
			// 要合併的點若沒問題則pushback
			if( p.z != 0 )
			{
				p.x /= count;
				p.y /= count;
				p.z /= count;
				cout << "merge index at " << _index << endl;
				//cout << endl << p.getVector3fMap() << endl;
				mvIndex.push_back(_index);
				mpcCurve->push_back(p);
			}
			//std::cout << p.x << ", " << p.y << ", " << p.z << endl; 
			
				
		}
		// 判斷只有單一區塊時的值是否合理(起始值誤差太大沒有加入判斷)
		else
		{
			//std::cout << "* 只有單一區塊" << std::endl;
			bool write_flag = false;
			// 有連續的曲線點,找前一點的深度值加以判斷是否合理
			float threshold;
			if (_pPointCloud->at(i).z < 1000)
				threshold = 0.25;
			else if (_pPointCloud->at(i).z < 2000)
				threshold = 0.15;
			else 
				threshold = 0.1;

			
			if( mvIndex.size() > 0 &&  
				abs(mvIndex.at(mvIndex.size()-1) - _vIndice.at(i)) <= 3 &&
				abs(mpcCurve->at(mpcCurve->size()-1).z - _pPointCloud->at(i).z) < _pPointCloud->at(i).z* threshold)
			{
				//std::cout << "** case 1" << std::endl;
				write_flag = true;
			}
			// 無連續點，則直接合理當曲線上的第一點
			else if ( mvIndex.size() > 0 &&
					  abs(mvIndex.at(mvIndex.size()-1) - _vIndice.at(i)) > 3 )
			{
				std::cout << "** case 2" << std::endl;
				write_flag = true;
			}
			//  一開始若都沒有點也直接寫入
			else if ( mvIndex.size() == 0 )
			{
				//std::cout << "** case 3" << std::endl;
				write_flag = true;
			}

			if (write_flag)
			{
				//cout << _index <<endl;
				pcl::PointXYZ p;
				p.getVector3fMap() = _pPointCloud->at(i).getVector3fMap();
				/*p.x = _pPointCloud->at(i).x;
				p.y = _pPointCloud->at(i).y;
				p.z = _pPointCloud->at(i).z;*/
				mvIndex.push_back(_index);
				mpcCurve->push_back(p);
			}
			else
			{
				;
				cout << "not on curve : "<< _index << endl;
			}
		}

		
		
			j ++;
			i = j;
			if (j < _vIndice.size() -1)
			{
			_indexPre = _vIndice.at(i);
			_index	  = _vIndice.at(j);
			}

	}
	std::cout << "* points after : " << mvIndex.size() << endl; 

	run(mvIndex, mpcCurve);

	//////////////////////////////////////////
	//		2.移除雜點&重新評估連續點		//
	//////////////////////////////////////////
	



	//////////////////////////////////////////
	//			3.移除彈跳點				//
	//////////////////////////////////////////
	//std::cout << "* mvIndex : " << mvIndex.size() << endl; 
	//std::cout << "* mpcCurve : " << mpcCurve->size() << endl;
	//
	//vector<int> vIndiceRemoveBounce;
	//pcl::PointCloud<pcl::PointXYZ> pcRemoveBounce;

	//int seq_index = 0;
	//pcl::PointXYZ pPrevious;
	//pcl::PointXYZ pCurrent;

	//int updown = 0; // 0 : not set, 1: up, 2: down
	//// 判斷上升or 下降
	//// 若下降則有可能有反彈點
	//// 判斷逆向反彈
	//for(int i = 0; i < mvIndex.size()-1; i++)
	//{
	//	cout << "handling" <<  mvIndex.at(i) << endl;
	//	int indexStart, indexEnd;
	//	int idexStartNew, indexEndNew;

	//	// 選取sequence範圍
	//	if (i == 0)
	//	{
	//		cout << "select seq..." << indexStart << " -> " << indexEnd << endl;
	//		indexStart = mvIndexPairs.at(seq_index).first;
	//		indexEnd   = mvIndexPairs.at(seq_index).second;
	//	}
	//	// 因為每次loop都是處理第i的點,所以做到i時就可以換下一個sequence
	//	else if( mvIndex.at(i) > indexEnd )
	//	{
	//		seq_index++;
	//		indexStart = mvIndexPairs.at(seq_index).first;
	//		indexEnd   = mvIndexPairs.at(seq_index).second;
	//		updown = 0;
	//		cout << "select seq..." << indexStart << " -> " << indexEnd << endl;
	//	}
	//	// 判斷
	//	if( mvIndex.at(i) == indexStart)
	//	{
	//		std::cout << "Initialization... " << endl; 
	//		pPrevious.getVector3fMap() = mpcCurve->at(i).getVector3fMap();
	//		//pCurrent.getVector3fMap() = _pPointCloud->at(i+1).getVector3fMap();
	//		pCurrent.getVector3fMap() = pPrevious.getVector3fMap();
	//	}
	//	else
	//	{
	//		
	//		pCurrent.getVector3fMap() = mpcCurve->at(i).getVector3fMap();
	//		std::cout << "previous y : " <<  pPrevious.y << ", " << "cuurent y : " << pCurrent.y << endl; 
	//		if( updown == 0 )
	//		{
	//			if( pCurrent.y - pPrevious.y >=0) // 上升
	//			{
	//				updown = 1;
	//			}
	//			else // 下降
	//			{
	//				updown = 2;
	//			}

	//		}
	//		else
	//		{
	//			if( updown == 1) 
	//			{
	//				if( pCurrent.y - pPrevious.y < 0 )//上升後開始下降
	//				{
	//					updown = 2;
	//				}
	//			}
	//			else if ( updown ==2) 
	//			{
	//				if( pCurrent.y - pPrevious.y > 0 )//反彈
	//				{
	//					cout << "Removing point after " << mvIndex.at(i) << " updown "<< updown << endl;
	//					indexEndNew = mvIndex.at(i-1);
	//					mvIndexPairs.at(seq_index).second = indexEndNew;
	//					std::cout << mvIndexPairs.at(seq_index).first << ", " << mvIndexPairs.at(seq_index).second << endl; 
	//				}
	//			}
	//		}
	//		

	//		pPrevious.getVector3fMap() = pCurrent.getVector3fMap();
	//	}
	//
	//	if( mvIndex.at(i) >= indexStart && mvIndex.at(i) < indexEndNew )
	//	{
	//		vIndiceRemoveBounce.push_back(mvIndex.at(i));
	//		pcRemoveBounce.push_back(pCurrent);
	//	}

	//	

	//}



	
	/*std::cout << mvIndex.size() << endl;
	for ( int i = 0; i < mvIndex.size(); i++)
	{
		std::cout << mvIndex.at(i) << ", " << mvIndex.at(i) << endl; 
	}*/
}

void CurveParsing::getSeq(int number, vector< std::pair<int,int> >& vOut, pcl::PointCloud<pcl::PointXYZ>::Ptr pOut )
{
	std::cout << "* getSeq" << endl;
	int iStart = mvIndexPairs2.at(number).first;
	int iEnd   = mvIndexPairs2.at(number).second;
	std::cout << iStart << ", " << iEnd << endl;
	
	if(mvIndex2.size() == 0)
		return ;

	for (int i = 0; i < mvIndex2.size(); i++)
	{
		if	( mvIndex2.at(i) >= iStart && mvIndex2.at(i) <= iEnd)
		{
			vOut.push_back(mvIndexPairs2.at(number));
			pOut->push_back(mpcCurve2->at(i));
		}
	}
}
void CurveParsing::getIndex(int number, vector<int>& vOut)
{
	for (int i = 0; i < mvIndex2.size(); i++)
	{
			vOut.push_back(mvIndex2.at(i));
	}
}

void CurveParsing::getPair( vector< std::pair<int,int> >& vOut )
{
	std::cout << "* getPair" << endl;
	for( vector< std::pair<int,int> >::iterator it = mvIndexPairs2.begin(); it != mvIndexPairs2.end(); ++it )
	{
		std::cout << (*it).first << ", " << (*it).second << endl;
		vOut.push_back(*it);
	}
}