#ifndef _CURVEPARSING_H_
#define _USE_MATH_DEFINES

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

using namespace std;

class CurveParsing{
public:	
	CurveParsing():mpcCurve(new pcl::PointCloud<pcl::PointXYZ>()), mpcCurve2(new pcl::PointCloud<pcl::PointXYZ>())
	{
	};
	~CurveParsing();
	void CurveParsing::run( vector<int>& _vIndice, pcl::PointCloud<pcl::PointXYZ>::Ptr _pPointCloud );
	void printResult();
	void removeRedundant( vector<int>& _vIndice, pcl::PointCloud<pcl::PointXYZ>::Ptr _pPointCloud );
	void getSeq(int number, vector< std::pair<int,int> >& vOut, pcl::PointCloud<pcl::PointXYZ>::Ptr pOut );
	void read(const char* _fileName, vector<int>& _vIndice, pcl::PointCloud<pcl::PointXYZ>::Ptr _pPointCloud );
	void getIndex(int number, vector<int>& vOut);
	void getPair( vector< std::pair<int,int> >& vOut);
private:
	vector<int> mvIndex;
	vector< std::pair<int, int> > mvIndexPairs;
	pcl::PointCloud<pcl::PointXYZ>::Ptr mpcCurve;

	vector<int> mvIndex2;
	vector< std::pair<int, int> > mvIndexPairs2;
	pcl::PointCloud<pcl::PointXYZ>::Ptr mpcCurve2;
};

#endif _CURVEPARSING_H_